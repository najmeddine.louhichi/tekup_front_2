import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  users:Observable<User[]>
  constructor (private _userService:UserService, private route:Router )  { }

  ngOnInit() {
    this.reloadData();
  }


  reloadData() {
    this.users = this._userService.getUsersList();
  
  
  }
  deleteUser(idUser:any) {
    this._userService.deleteUser(idUser)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}
