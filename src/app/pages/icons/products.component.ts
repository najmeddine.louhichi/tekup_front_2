import {Component, OnInit} from '@angular/core';
import {ProductService} from 'src/app/product.service';
import {Router} from '@angular/router';

import {Observable} from 'rxjs';
import {Product} from '../../model/Product';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
    products: Observable<Product[]>;

    constructor(private _productService: ProductService, private route: Router) {
    }

    ngOnInit() {
        this.reloadData();
    }


    reloadData() {
        this.products = this._productService.getProductsList();


    }

    deleteProduct(id: any) {
        this._productService.deleteProduct(id)
            .subscribe(
                data => {
                    console.log(data);
                    this.reloadData();
                },
                error => console.log(error));
    }


}
