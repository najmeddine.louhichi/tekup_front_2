import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Category, SubCategory} from '../model/Category';
import {CategoryService} from '../category.service';

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {

  id:any;
  category: Category;
  submitted = false;
  constructor(private route: ActivatedRoute, private _categoryService: CategoryService, private router: Router) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['idCat'];
    this._categoryService.getCategory(this.id).subscribe(
        data =>{
          this.category = data;
          if(typeof this.category.subCategories === 'undefined'){
            this.category.subCategories = [];
          }
        }
    );
  }


  save() {
    this._categoryService.updateCategory(this.id, this.category)
        .subscribe(data => {
          this.category = new Category();
          this.gotoList();
        }, error => console.log(error));

  }
  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/categoryBrand']);
  }
  addSubCategory(){
    this.category.subCategories.push(new SubCategory());
  }

  removeSubCategory(i){
    this.category.subCategories.splice(i, 1);
  }
}
