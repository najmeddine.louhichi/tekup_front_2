import {Component, OnInit} from '@angular/core';
import {Product} from '../model/Product';
import {ProductService} from '../product.service';
import {Router} from '@angular/router';
import {CategoryService} from '../category.service';

@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

    product: Product = new Product();
    submitted = false;
    categoryList = [];

    constructor(private _productService: ProductService, private router: Router, private categoryService: CategoryService) {
    }

    ngOnInit(): void {
        this.categoryService.getCategorysList().subscribe(data => this.categoryList = data);
    }

    save() {
        console.log(this.product);
        this._productService.createProduct(this.product)
            .subscribe(data => console.log(data), error => console.log(error));
        this.product = new Product();
        this.gotoList();
    }

    onSubmit() {
        this.submitted = true;
        this.save();
    }

    gotoList() {
        this.router.navigate(['/dashboard']);
    }

}
