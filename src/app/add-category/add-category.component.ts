import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CategoryService} from '../category.service';
import {Category, SubCategory} from '../model/Category';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {
  category: Category = new Category();
  submitted = false;

  constructor(private _categoryService: CategoryService, private router: Router) {
    this.category.subCategories = [];
  }

  ngOnInit(): void {
  }


  save() {
    this._categoryService.createCategory(this.category)
        .subscribe(data => {
          this.category = new Category();
          this.gotoList();
        }, error => console.log(error));

  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/categoryBrand']);
  }

  addSubCategory() {
    this.category.subCategories.push(new SubCategory());
  }

  removeSubCategory(i) {
    this.category.subCategories.splice(i, 1);
  }
}
