import {Component, OnInit} from '@angular/core';
import {ProductService} from 'src/app/product.service';
import {CartService} from 'src/app/cart.service';
import {Product} from '../../../model/Product';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
    public products: Array<Product>;

    private sub;

    constructor(private productService: ProductService, private cartService: CartService
    ) {
    }

    ngOnInit(): void {
        this.sub = this.productService.getProductsList()
            .subscribe(res => {
              console.log(res);
                this.products = res;
            });
    }

    addToCart = (product) => {
        this.cartService.addToCart({product, quantity: 1});
    };

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
