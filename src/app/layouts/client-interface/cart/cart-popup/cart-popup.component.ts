/**
 * Created by andrew.yang on 7/28/2017.
 */

import {Component, HostBinding, ElementRef, OnInit} from '@angular/core';
import {CartBaseComponent} from '../cart-base.component';
import { CartService } from 'src/app/cart.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'cart-popup',
    styleUrls: ['cart-popup.component.css'],
    templateUrl: 'cart-popup.component.html',
    host: {
        '(document:click)': 'onPageClick($event)',
    }
})
export class CartPopupComponent extends CartBaseComponent implements OnInit{
    @HostBinding('class.visible') isVisible = false;

    constructor(
        protected cartService: CartService,
        private eleref: ElementRef
    ) {
        super(cartService);
    }
    ngOnInit() {
        this.cartService.toggleCartSubject.subscribe(res => {
            this.isVisible = res;
        });
    }
    onPageClick = (event) => {
        if (this.isVisible && !this.eleref.nativeElement.contains(event.target) && event.target.className !== 'cart-remove') {
            this.cartService.toggleCart();
        }
    }
}
