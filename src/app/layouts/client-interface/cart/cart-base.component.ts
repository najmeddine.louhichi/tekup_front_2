import { Cart } from 'src/app/model/cart';
import { CartService } from 'src/app/cart.service';

/**
 * Created by Andrew on 7/30/2017.
 */
export class CartBaseComponent {
    public cartList: Cart[];
    public totalPrice: number;
    constructor(protected cartService: CartService) {
        this.loadCart();
    }
    loadCart = () => {
        this.cartService.cartListSubject
            .subscribe(res => {
                this.cartList = res;
                let total = 0;
                for (const cart of this.cartList) {
                    total += cart.product.prix * cart.quantity;
                }
                this.totalPrice = total;
            });
    }
    removeFromCart = index => {
        this.cartService.removeCart(index);
    }
}
