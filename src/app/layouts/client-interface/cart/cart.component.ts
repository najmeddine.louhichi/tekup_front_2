/**
 * Created by andrew.yang on 7/31/2017.
 */
import { Component } from '@angular/core';
import {CartBaseComponent} from "./cart-base.component";
import { CartService } from 'src/app/cart.service';

@Component({
    selector: 'app-cart-page',
    styleUrls: ["cart.component.css"],
    templateUrl: 'cart.component.html'
})
export class CartComponent extends CartBaseComponent{
    constructor(protected cartService: CartService) {
        super(cartService);
    }

    ngOnInit() {

    }
    changeQuantity = (cart,quantity) => {
        cart.quantity = quantity;
        this.cartService.reloadCart(this.cartList);
    }
}
