import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/product.service';
import { CartService } from 'src/app/cart.service';
import {Product} from '../../../model/Product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  private sub;
  public product: Product;
  quantity = 1;
  constructor(private route: ActivatedRoute,
              private productService: ProductService,
              private cartService: CartService
  ) { }

  ngOnInit() {
      this.route.params
          .subscribe(res => {
              this.getProduct(res.id);
          });
  }
  getProduct(id) {
      this.sub = this.productService.getProductsList()
          .subscribe(res => {
              this.product = res[id - 1];
          });
  }
  changeQuantity = (newQuantity: number) => {
      this.quantity = newQuantity;
  }
  addToCart = (product) => {
      if (this.quantity) { this.cartService.addToCart({product, quantity: this.quantity}); }
  }
  ngOnDestroy() {
      this.sub.unsubscribe();
  }

}
