import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Category} from '../model/Category';
import {CategoryService} from '../category.service';


@Component({
    selector: 'app-category-brand',
    templateUrl: './category-brand.component.html',
    styleUrls: ['./category-brand.component.css']
})
export class CategoryBrandComponent implements OnInit {
    categories: Category[];
    maxNumberOfSubCategories = 1;
    subCategoryIndices = [];

    constructor(private _categoryService: CategoryService, private route: Router) {
    }

    ngOnInit() {
        this.reloadData();
    }


    reloadData() {
        this._categoryService.getCategorysList().subscribe(
            (data: Category[]) => {

                data.forEach(category => {
                    if (category.subCategories.length > this.maxNumberOfSubCategories) {
                        this.maxNumberOfSubCategories = category.subCategories.length;
                    }
                });
                this.subCategoryIndices = Array.from({length: this.maxNumberOfSubCategories}, (_, i) => i);
                console.log(this.subCategoryIndices);
                this.categories = data;
            }
        );


    }

    deleteCategory(idCat: any) {
        this._categoryService.deleteCategory(idCat)
            .subscribe(
                data => {
                    console.log(data);
                    this.reloadData();
                },
                error => console.log(error));
    }


}
